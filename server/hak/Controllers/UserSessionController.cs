﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hak.Data;
using hak.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace hak.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserSessionController : ControllerBase
    {
        [Route("api/[controller]")]
        [ApiController]
        public class QuizController : ControllerBase
        {
            DatabaseContext db;
            public QuizController(DatabaseContext context)
            {
                this.db = context;
            }

            [HttpGet]
            public IEnumerable<UserSession> Get()
            {
                return db.UserSessions.ToList();
            }

            // GET api/UserSessions/5
            [HttpGet("{id}")]
            public IActionResult Get(int id)
            {
                UserSession UserSession = db.UserSessions.FirstOrDefault(x => x.Id == id);
                if (UserSession == null)
                    return NotFound();
                return new ObjectResult(UserSession);
            }

            // POST api/UserSessions
            [HttpPost]
            public IActionResult Post([FromBody]UserSession UserSession)
            {
                if (UserSession == null)
                {
                    return BadRequest();
                }

                db.UserSessions.Add(UserSession);
                db.SaveChanges();
                return Ok(UserSession);
            }

            // PUT api/UserSessions/
            [HttpPut]
            public IActionResult Put([FromBody]UserSession UserSession)
            {
                if (UserSession == null)
                {
                    return BadRequest();
                }
                if (!db.UserSessions.Any(x => x.Id == UserSession.Id))
                {
                    return NotFound();
                }

                db.Update(UserSession);
                db.SaveChanges();
                return Ok(UserSession);
            }

            // DELETE api/UserSessions/5
            [HttpDelete("{id}")]
            public IActionResult Delete(int id)
            {
                UserSession UserSession = db.UserSessions.FirstOrDefault(x => x.Id == id);
                if (UserSession == null)
                {
                    return NotFound();
                }
                db.UserSessions.Remove(UserSession);
                db.SaveChanges();
                return Ok(UserSession);
            }
        }
    }
}
