const languageContainerMap = {
  'javascript': 'code-runner-node',
  'java': 'code-runner-java',
};

module.exports = {
  languageContainerMap,
};
