﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hak.Data;
using hak.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace hak.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuizController : ControllerBase
    {
        DatabaseContext db;
        public QuizController(DatabaseContext context)
        {
            this.db = context;
        }

        [HttpGet]
        public IEnumerable<Quiz> Get()
        {
            return db.Quizs.ToList();
        }

        // GET api/Quizs/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Quiz Quiz = db.Quizs.FirstOrDefault(x => x.Id == id);
            if (Quiz == null)
                return NotFound();
            return new ObjectResult(Quiz);
        }

        // POST api/Quizs
        [HttpPost]
        public IActionResult Post([FromBody]Quiz Quiz)
        {
            if (Quiz == null)
            {
                return BadRequest();
            }

            db.Quizs.Add(Quiz);
            db.SaveChanges();
            return Ok(Quiz);
        }

        // PUT api/Quizs/
        [HttpPut]
        public IActionResult Put([FromBody]Quiz Quiz)
        {
            if (Quiz == null)
            {
                return BadRequest();
            }
            if (!db.Quizs.Any(x => x.Id == Quiz.Id))
            {
                return NotFound();
            }

            db.Update(Quiz);
            db.SaveChanges();
            return Ok(Quiz);
        }

        // DELETE api/Quizs/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Quiz Quiz = db.Quizs.FirstOrDefault(x => x.Id == id);
            if (Quiz == null)
            {
                return NotFound();
            }
            db.Quizs.Remove(Quiz);
            db.SaveChanges();
            return Ok(Quiz);
        }
    }
}
