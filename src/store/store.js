import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

const rootReducer = combineReducers({
  // import reducer
});

const initState = {
 // import initial state
};

export const store = createStore(
  rootReducer,
  initState,
  composeWithDevTools(applyMiddleware(thunk)),
);
