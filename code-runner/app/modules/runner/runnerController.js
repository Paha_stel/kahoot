const RunnerParser = require('./runnerParser');
const RunnerQueue = require('./runnerQueue');
const runnerRepository = require('./runnerRepository');

const runnerParser = new RunnerParser();
const runnerQueue = new RunnerQueue(
  ({id, output, error}) => {
    const parsedOutput = runnerParser.format(output);

    const status = error ? 'error' : 'completed';

    runnerRepository.add(id, {
      ...parsedOutput,
      status,
      executionError: error
    })
  }
);

const addRunner = (req, res, next) => {
  const runnerUuid = runnerQueue.add(req.body);

  res.status(202).send({uuid: runnerUuid});
};

const getRunner = (req, res) => {
  const runner = runnerRepository.get(req.params.id);
  if (runner) {
    res.status(200).send(runner);
  }
};

module.exports = {
  addRunner,
  getRunner,
};
