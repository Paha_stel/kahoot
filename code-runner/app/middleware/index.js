const errorHandler = require('./errorHandler');
const morgan = require('./morgan');

module.exports = {
  errorHandler,
  morgan,
}
