const { Router } = require('express');
const { runnerRouter } = require('./runner');

const router = Router();

router.use('/runner', runnerRouter);

module.exports = router;
