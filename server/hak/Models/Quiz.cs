﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hak.Models
{
    public class Quiz
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
        public string Code { get; set; }
        public string TestCode { get; set; }

        public Quiz()
        {
        }

        public Quiz(int id, string name, string discription, string code, string testCode)
        {
            Id = id;
            Name = name;
            Discription = discription;
            Code = code;
            TestCode = testCode;
        }
    }
}
