const Joi = require('@hapi/joi');

const { addRunnerSchema } = require('./runnerSchema');

const validateAddRunner = (req, res, next) => {
  const { error } = Joi.validate(req.body, addRunnerSchema, { stripUnknown: true });

  if (error) {
    return next(error);
  } else {
    return next();
  }
};

module.exports = {
  validateAddRunner,
};
