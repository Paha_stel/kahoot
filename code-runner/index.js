const app = require('./app');

const PORT = process.env.CODE_RUNNER_PORT || 3003;

app.listen(PORT, () => console.log(`Server has been started on ${PORT}`));
