﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hak.Models
{
    public class UserSession
    {
        [Key]
        public int Id { get; set; }
        public string Status { get; set; }

        public int QuizId { get; set; }
        public virtual Quiz Quiz { get; set; }

        public int SessionId { get; set; }

        public UserSession()
        {
        }

        public UserSession(int id, string status, Quiz quiz, int sessionId)
        {
            Id = id;
            Status = status;
            Quiz = quiz;
            SessionId = sessionId;
        }
    }
}
