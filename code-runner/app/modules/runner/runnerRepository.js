const store = {};

const get = id => {
  return store[id];
};

const add = (id, result) => {
  store[id] = result;
}

module.exports = {
  get,
  add,
};
