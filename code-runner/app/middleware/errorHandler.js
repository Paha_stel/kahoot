const errorHandler = (error, req, res, next) => {
  let status;
  switch (true) {
    case error.isJoi:
      status = 400;
      break;
    case Boolean(error.status):
      status = error.status;
      break;
    default:
      status = 500;
  }
  console.error(error);
  return res.status(status).send(error);
}

module.exports = errorHandler;
