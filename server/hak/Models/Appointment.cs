﻿using System;
using System.ComponentModel.DataAnnotations;

namespace hak.Models
{
    public class Appointment
    {
        /*public int Id { get; set; }
        [Display(Name = "Клиент")]
        public int ClientId { get; set; }
        public virtual Client Client { get; set; }

        [Display(Name = "Услуга")]
        public int ServiceId { get; set; }
        public virtual Service Service { get; set; }

        [Display(Name = "Сотрудник")]
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        [Display(Name = "Скидка")]
        public int Discount { get; set; }
        [Display(Name = "Описание")]
        public string Discription { get; set; }
        [Display(Name = "Время")]
        public DateTime Date { get; set; }
           
        public Appointment() { }

        public Appointment(int ClientId, int ServiceId, int EmployeeId, int Discount,
            string Discription, DateTime Date)
        {
            this.ClientId = ClientId;
            this.ServiceId = ServiceId;
            this.EmployeeId = EmployeeId;
            this.Discount = Discount;
            this.Discription = Discription;
            this.Date = Date;
        }

        public override bool Equals(object obj)
        {
            var item = obj as Appointment;

            if (obj == null)
            {
                return false;
            }
            if (obj == this)
            {
                return true;
            }

            return this.Id == item.Id;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }*/
    }
}
