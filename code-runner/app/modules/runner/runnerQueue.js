const uuidv1 = require('uuid/v1');

const docker = require('../../utils/docker');
const { languageContainerMap } = require('./constants');

class RunnerQueue {
  constructor(processResult) {
    this.processResult = processResult;
    this.queue = [];
  }

  async process() {
    if (this.isProcessing() || this.isEmpty()) return;

    const runner = this.queue[0];
    runner.isProcessing = true;

    try {
      const containerName = languageContainerMap[runner.language];
      const result = await docker.executeContainer(containerName, {
        Cmd: ['node', 'run-json', JSON.stringify(runner)],
        AttachStdout: true,
        AttachStderr: true
      });

      this.processResult && this.processResult({id: runner.uuid, ...result}) ;
    } catch (e) {
      throw(e);
    } finally {
      this.queue.shift();
      this.process();
    }
  }

  add(runner) {
    const uuid = uuidv1();
    const uniqueRunner = {
      uuid,
      ...runner
    };

    this.queue.push(uniqueRunner);
    this.process();

    return uuid;
  }

  isProcessing() {
    return this.queue.some(i => i.isProcessing);
  }

  isEmpty() {
    return this.queue.length === 0;
  }
}

module.exports = RunnerQueue;
