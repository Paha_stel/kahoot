﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hak.Models
{
    public class Service
    {
        [Key]
        public int ServiceId { get; set; }
        [Display(Name = "Услуга")]
        [Required(ErrorMessage = "Имя не указано")]
        public string Name { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Не указана цена")]
        public double Price { get; set; }

        public ICollection<Appointment> Appointments { get; set; }

        public Service() { }

        public Service(int ServiceId, string Name, string Description, double Price)
        {
            this.ServiceId = ServiceId;
            this.Name = Name;
            this.Description = Description;
            this.Price = Price;
        }

        public override bool Equals(object obj)
        {
            var item = obj as Service;

            if (obj == null)
            {
                return false;
            }
            if (obj == this)
            {
                return true;
            }

            return this.ServiceId == item.ServiceId;
        }

        public override int GetHashCode()
        {
            return this.ServiceId.GetHashCode();
        }
    }
}
