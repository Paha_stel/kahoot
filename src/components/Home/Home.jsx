import React from 'react';

import styles from './Home.module.scss';

export const Home = () => (
  <div className={styles.app}>
    hello world
  </div>
);
