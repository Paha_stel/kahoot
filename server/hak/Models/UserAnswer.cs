﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hak.Models
{
    public class UserAnswer
    {
        [Key]
        public int Id { get; set; }
        public int Time { get; set; }
        public int UserSessionId { get; set; }
        public virtual UserSession UserSession { get; set; }

        public UserAnswer()
        {
        }

        public UserAnswer(int id, int time, UserSession userSession)
        {
            Id = id;
            Time = time;
            UserSession = userSession;
        }
    }
}
