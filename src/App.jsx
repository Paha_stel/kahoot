import React from 'react';
import { Provider } from 'react-redux';

import { Home } from 'components/Home';
import { store } from 'store';

export const App = () => (
  <Provider store={store}>
    <Home />
  </Provider>
);
