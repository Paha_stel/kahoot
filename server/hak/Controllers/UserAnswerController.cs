﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hak.Data;
using hak.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace hak.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAnswerController : ControllerBase
    {

        DatabaseContext db;
        public UserAnswerController(DatabaseContext context)
        {
            this.db = context;
        }

        [HttpGet]
        public IEnumerable<UserAnswer> Get()
        {
            return db.UserAnswers.ToList();
        }

        // GET api/UserAnswers/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            UserAnswer UserAnswer = db.UserAnswers.FirstOrDefault(x => x.Id == id);
            if (UserAnswer == null)
                return NotFound();
            return new ObjectResult(UserAnswer);
        }

        // POST api/UserAnswers
        [HttpPost]
        public IActionResult Post([FromBody]UserAnswer UserAnswer)
        {
            if (UserAnswer == null)
            {
                return BadRequest();
            }

            db.UserAnswers.Add(UserAnswer);
            db.SaveChanges();
            return Ok(UserAnswer);
        }

        // PUT api/UserAnswers/
        [HttpPut]
        public IActionResult Put([FromBody]UserAnswer UserAnswer)
        {
            if (UserAnswer == null)
            {
                return BadRequest();
            }
            if (!db.UserAnswers.Any(x => x.Id == UserAnswer.Id))
            {
                return NotFound();
            }

            db.Update(UserAnswer);
            db.SaveChanges();
            return Ok(UserAnswer);
        }

        // DELETE api/UserAnswers/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            UserAnswer UserAnswer = db.UserAnswers.FirstOrDefault(x => x.Id == id);
            if (UserAnswer == null)
            {
                return NotFound();
            }
            db.UserAnswers.Remove(UserAnswer);
            db.SaveChanges();
            return Ok(UserAnswer);
        }
    }
}
