﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using hak.Data;
using hak.Models;

namespace hak.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        DatabaseContext db;
        public ValuesController(DatabaseContext context)
        {
            this.db = context;
            DbInitializer.Init(db);
        }

        [HttpGet]
        public IEnumerable<Service> Get()
        {
            return db.Services.ToList();
        }

        // GET api/Services/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Service Service = db.Services.FirstOrDefault(x => x.ServiceId == id);
            if (Service == null)
                return NotFound();
            return new ObjectResult(Service);
        }

        // POST api/Services
        [HttpPost]
        public IActionResult Post([FromBody]Service Service)
        {
            if (Service == null)
            {
                return BadRequest();
            }

            db.Services.Add(Service);
            db.SaveChanges();
            return Ok(Service);
        }

        // PUT api/Services/
        [HttpPut]
        public IActionResult Put([FromBody]Service Service)
        {
            if (Service == null)
            {
                return BadRequest();
            }
            if (!db.Services.Any(x => x.ServiceId == Service.ServiceId))
            {
                return NotFound();
            }

            db.Update(Service);
            db.SaveChanges();
            return Ok(Service);
        }

        // DELETE api/Services/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Service Service = db.Services.FirstOrDefault(x => x.ServiceId == id);
            if (Service == null)
            {
                return NotFound();
            }
            db.Services.Remove(Service);
            db.SaveChanges();
            return Ok(Service);
        }
    }
}
