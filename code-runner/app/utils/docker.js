const Docker = require('dockerode');
const { PassThrough } = require('stream');

const CONTAINER_STATUS = {
  RUNNING: 'running',
};

const docker = new Docker({
  socketPath: '/var/run/docker.sock'
});

const getContainer = (id) => docker.getContainer(id);

const isContainerAvailable = async (id) => {
  let containers;

  try {
    containers = await docker.listContainers();
  } catch (e) {
    throw(e);
  }

  return containers.some((containerInfo) => {
    const isNodeRunner = containerInfo.Names.some((name) => {
      return name.includes(id);
    });

    return isNodeRunner && containerInfo.State === CONTAINER_STATUS.RUNNING;
  });
};

const runContainer = async (id) => {
  let container = getContainer(id);

  try {
    await container.start();
  } catch(e) {
    throw(e);
  }
};

const executeContainer = async (id, opts = {}) => {
  let isAvailable;
  try {
    isAvailable = await isContainerAvailable(id);
  } catch (e) {
    throw(e);
  }

  try {
    if (!isAvailable) await runContainer(id);
  } catch (e) {
    throw(e);
  }

  const container = getContainer(id);

  let execution;
  try {
    execution = await container.exec(opts);
  } catch (e) {
    throw(e);
  }

  let stream;
  try {
    stream = await execution.start();
  } catch (e) {
    throw(e);
  }

  const resultStream = new PassThrough();
  const errorStream = new PassThrough();

  let output = '';
  resultStream.on('data', chunk => {
    output += chunk;
  });

  let error = '';
  errorStream.on('data', chunk => {
    error += chunk;
  });

  const result = new Promise((resolve, reject) => {
    stream.output.on('end', () => resolve({
      output,
      error
    }));
  })

  container.modem.demuxStream(stream.output, resultStream, errorStream);

  return result;
};

module.exports = {
  getContainer,
  isContainerAvailable,
  runContainer,
  executeContainer,
};
