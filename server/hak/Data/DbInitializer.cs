﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hak.Models;

namespace hak.Data
{
    public class DbInitializer
    {
        public static void Init(DatabaseContext db)
        {
            db.Database.EnsureCreated();

            if (db.Clients.Any())
            {
                return;
            }
            int clietnCount = 7;
            int employeeCount = 5;
            int servicesCount = 7;
            int appointmentCount = 7;

            string[] names = { "Аким", "Богдан", "Фока", "Петр", "Семён",
                "Ксения", "Лилия", "Ангелина", "Анна", "Инна" };
            string[] surnames = { "Крутелев", "Игошин", "Урусов", "Щередин", "Мандрыко",
                "Баркова", "Кандинская", "Козариса", "Каца", "Хадеева" };
            string[] posts = { "Парикмахер", "Стилист", "Администратор", "Заведующий складом" };
            string[] servicesNames = { "Стрижка бороды", "Стрижки наголо", "Модельные мужские стрижки", "Укладка", "Окрашивания" };

            Random rand = new Random();

            for (int i = 0; i < clietnCount; i++)
            {
                StringBuilder phone = new StringBuilder("+");
                for (int k = 0; k < 9; k++)
                {
                    phone.Append(rand.Next(10).ToString());
                }
                db.Clients.Add(new Client
                {
                    Name = names[rand.Next(names.Length)],
                    Phone = phone.ToString()
                });
            }
            db.SaveChanges();

            for (int i = 0; i < employeeCount; i++)
            {
                StringBuilder phone = new StringBuilder("+");
                for (int k = 0; k < 9; k++)
                {
                    phone.Append(rand.Next(10).ToString());
                }
                db.Employees.Add(new Employee
                {
                    Name = names[rand.Next(names.Length)],
                    Surname = surnames[rand.Next(surnames.Length)],
                    Phone = phone.ToString(),
                    Post = posts[rand.Next(posts.Length)],
                    Salary = rand.Next(200) + 200
                });
            }
            db.SaveChanges();

            for (int i = 0; i < servicesCount; i++)
            {
                db.Services.Add(new Service
                {
                    Name = servicesNames[rand.Next(servicesNames.Length)],
                    Price = rand.Next(200) + 200
                });
            }
            db.SaveChanges();

            for (int i = 0; i < appointmentCount; i++)
            {
                DateTime start = new DateTime(2018, 1, 1);
                int range = (DateTime.Today - start).Days;

                db.Appointments.Add(new Appointment
                {
                    ClientId = rand.Next(clietnCount) + 1,
                    EmployeeId = rand.Next(employeeCount) + 1,
                    ServiceId = rand.Next(servicesCount) + 1,
                    Discount = rand.Next(5),
                    Date = start.AddDays(rand.Next(range))
                });
            }
            db.SaveChanges();
        }
    }
}
