const { Router } = require('express');
const timeout = require('connect-timeout');

const { validateAddRunner } = require('./runnerValidation');
const { addRunner, getRunner } = require('./runnerController');

const router = Router();

router
  .get('/:id', timeout('15s'), getRunner)
  .post('/', validateAddRunner, addRunner);

module.exports = router;
