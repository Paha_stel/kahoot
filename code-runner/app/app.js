const express = require('express');
const bodyParser  = require('body-parser');

const { errorHandler, morgan } = require('./middleware');
const api = require('./modules');

const app = express();

app.use(morgan);

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use('/api/v1', api);

app.use(errorHandler);

module.exports = app;
