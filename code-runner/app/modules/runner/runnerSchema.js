const Joi = require('@hapi/joi');

const { languageContainerMap } = require('./constants');

const addRunnerSchema = Joi.object().keys({
  language: Joi.string().valid(Object.keys(languageContainerMap)).required(),
  code: Joi.string().required(),
  fixture: Joi.string().required(),
  testFramework: Joi.string().required(),
  languageVersion: Joi.string(),
});

module.exports = {
  addRunnerSchema,
};
